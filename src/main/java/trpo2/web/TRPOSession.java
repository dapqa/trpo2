package trpo2.web;

import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;
import trpo2.core.dto.Account;

/**
 * @author Nechaev Alexander
 */
public class TRPOSession extends WebSession {

    private Account account;

    public TRPOSession(Request request) {
        super(request);
    }

    public static TRPOSession get() {
        return (TRPOSession) Session.get();
    }

    public boolean isSignedIn() {
        return account != null;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
