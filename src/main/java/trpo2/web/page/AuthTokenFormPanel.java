package trpo2.web.page;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import trpo2.core.dto.AuthToken;
import trpo2.web.TRPOSession;

/**
 * @author Nechaev Alexander
 */
abstract class AuthTokenFormPanel extends Panel {

    public AuthTokenFormPanel(String id) {
        super(id);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        Form<AuthToken> authTokenForm = new Form<>("authTokenForm", new CompoundPropertyModel<>(new AuthToken()));
        authTokenForm.setVisible(!TRPOSession.get().isSignedIn());
        add(authTokenForm);

        FeedbackPanel feedbackPanel = new FeedbackPanel("feedback");
        feedbackPanel.setOutputMarkupId(true);
        authTokenForm.add(feedbackPanel);

        authTokenForm.add(new TextField<>("username").setRequired(true).setLabel(Model.of("Логин")));
        authTokenForm.add(new PasswordTextField("password").setRequired(true).setLabel(Model.of("Пароль")));

        authTokenForm.add(new AjaxSubmitLink("submit") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                if (!form.hasError()) {
                    AuthToken authToken = authTokenForm.getModelObject();
                    try {
                        AuthTokenFormPanel.this.onSubmit(authToken);
                    } catch (Exception e) {
                        authTokenForm.error(e.getMessage());
                        target.add(feedbackPanel);
                    }
                }
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onInitialize() {
                super.onInitialize();

                add(new Label("submitLabel", getSubmitLabelModel()));
            }
        });
    }

    protected abstract void onSubmit(AuthToken authToken);

    protected abstract IModel<String> getSubmitLabelModel();

}
