package trpo2.web.page;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.*;
import org.apache.wicket.spring.injection.annot.SpringBean;
import trpo2.core.dto.AuthToken;
import trpo2.core.dto.Message;
import trpo2.core.service.AccountService;
import trpo2.core.service.MessageService;
import trpo2.web.TRPOSession;
import trpo2.web.behavior.ModelUpdateBehavior;
import trpo2.web.page.base.BasePage;
import trpo2.web.panel.MessagePanel;

import java.util.List;

/**
 * @author Nechaev Alexander
 */
public class DashboardPage extends BasePage {

    @SpringBean(name = "messageService")
    private MessageService messageService;

    @SpringBean(name = "accountService")
    private AccountService accountService;

    private Form<Message> form;
    private FeedbackPanel feedbackPanel;
    private WebMarkupContainer messagesPaneContainer;

    @Override
    protected void onInitialize() {
        super.onInitialize();

        createSignInPanel();
        createSignUpPanel();

        form = new Form<>("newMessageForm", new CompoundPropertyModel<>(new Message()));
        form.setOutputMarkupId(true);
        form.setVisible(TRPOSession.get().isSignedIn());
        add(form);

        form.add(feedbackPanel = new FeedbackPanel("feedback"));
        feedbackPanel.setOutputMarkupId(true);

        form.add(new DropDownChoice<>("messageColor", messageService.getMessageColors())
                .setNullValid(true)
                .setChoiceRenderer(new ChoiceRenderer<>("name", "id"))
                .setLabel(Model.of("Цвет"))
                .add(new ModelUpdateBehavior())
        );

        form.add(new Label("login", new PropertyModel<>(TRPOSession.get().getAccount(), "login")));
        form.add(new AjaxLink<Void>("logout") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                TRPOSession.get().setAccount(null);
                setResponsePage(DashboardPage.class);
            }
        });

        form.add(new TextArea<>("content")
                .setRequired(true)
                .setLabel(Model.of("Сообщение"))
                .add(new ModelUpdateBehavior())
        );

        form.add(new AjaxSubmitLink("send") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                if (!form.hasError()) {
                    Message message = DashboardPage.this.form.getModelObject();
                    message.setAuthor(TRPOSession.get().getAccount());

                    messageService.postMessage(message);
                    DashboardPage.this.form.setModelObject(new Message());

                    target.add(form, feedbackPanel, messagesPaneContainer);
                }
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }
        });

        ListView<Message> messageListView = new ListView<Message>(
                "messagesPane",
                new AbstractReadOnlyModel<List<Message>>() {
                    @Override
                    public List<Message> getObject() {
                        return messageService.getMessages();
                    }
                }
        ) {
            @Override
            protected void populateItem(ListItem<Message> listItem) {
                listItem.add(new MessagePanel("message", listItem.getModel()));
            }
        };

        add(messagesPaneContainer = new WebMarkupContainer("messagesPaneContainer"));
        messagesPaneContainer.setOutputMarkupId(true);
        messagesPaneContainer.add(messageListView);
    }

    private void createSignInPanel() {
        add(new AuthTokenFormPanel("signInPanel") {
            @Override
            protected void onSubmit(AuthToken authToken) {
                Long id = accountService.signIn(authToken.getUsername(), authToken.getPassword());
                TRPOSession.get().setAccount(accountService.getAccountById(id));
                setResponsePage(DashboardPage.class);
            }

            @Override
            protected IModel<String> getSubmitLabelModel() {
                return Model.of("Войти");
            }

            @Override
            protected void onConfigure() {
                super.onConfigure();
                setVisible(!TRPOSession.get().isSignedIn());
            }
        });
    }

    private void createSignUpPanel() {
        add(new AuthTokenFormPanel("signUpPanel") {
            @Override
            protected void onSubmit(AuthToken authToken) {
                Long id = accountService.signUp(authToken.getUsername(), authToken.getPassword());
                TRPOSession.get().setAccount(accountService.getAccountById(id));
                setResponsePage(DashboardPage.class);
            }

            @Override
            protected IModel<String> getSubmitLabelModel() {
                return Model.of("Зарегистрироваться");
            }

            @Override
            protected void onConfigure() {
                super.onConfigure();
                setVisible(!TRPOSession.get().isSignedIn());
            }
        });
    }

}
