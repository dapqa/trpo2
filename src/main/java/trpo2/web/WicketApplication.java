package trpo2.web;

import org.apache.wicket.Session;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import trpo2.web.page.DashboardPage;

public class WicketApplication extends WebApplication {

	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return DashboardPage.class;
	}

	@Override
	protected void init() {
		super.init();

		getComponentInstantiationListeners().add(new SpringComponentInjector(this));
		getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
	}

	@Override
	public Session newSession(Request request, Response response) {
		return new TRPOSession(request);
	}
}
