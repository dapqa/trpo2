package trpo2.jdbc.base;

import trpo2.core.dto.base.BaseDTO;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.Serializable;

/**
 * @author Nechaev Alexander
 */
public class BaseService implements Serializable {

    protected TRPO2JdbcTemplate jdbcTemplate;

    @Resource
    public final void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new TRPO2JdbcTemplate(dataSource);
    }

    protected <T extends BaseDTO> boolean isNew(T dto) {
        return dto.getId() == null;
    }

}