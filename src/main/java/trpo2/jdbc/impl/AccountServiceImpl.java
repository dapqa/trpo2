package trpo2.jdbc.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import trpo2.core.dto.Account;
import trpo2.core.service.AccountService;
import trpo2.jdbc.base.BaseService;
import trpo2.util.CommonUtils;

/**
 * @author Nechaev Alexander
 */
@Service("accountService")
public class AccountServiceImpl extends BaseService implements AccountService {

    private static final RowMapper<Account> accountRowMapper = (rs, i) -> {
        Account account = new Account();

        account.setId(rs.getLong("id"));
        account.setLogin(rs.getString("login"));

        return account;
    };

    @Override
    @Transactional
    public Long signUp(String username, String password) {
        long loginCount = jdbcTemplate.queryForObject(
                "select count(*) from account_v where lower(login) = ?",
                Long.class,
                username
        );
        if (loginCount > 0) {
            throw new IllegalArgumentException("Пользователь с таким логином уже существует");
        }

        return jdbcTemplate.insertReturnId(
                "insert into account(login, password_hash) values(?, ?)",
                username,
                CommonUtils.encryptMD5(password)
        );
    }

    @Override
    @Transactional(readOnly = true)
    public Long signIn(String username, String password) {
        long loginCount = jdbcTemplate.queryForObject(
                "select count(*) from account_v where lower(login) = ? and password_hash = ?",
                Long.class,
                username,
                CommonUtils.encryptMD5(password)
        );
        if (loginCount != 1) {
            throw new IllegalArgumentException("Неправильный логин или пароль");
        }

        return jdbcTemplate.queryForObject(
                "select id from account_v where lower(login) = ? and password_hash = ?",
                Long.class,
                username,
                CommonUtils.encryptMD5(password)
        );
    }

    @Override
    @Transactional(readOnly = true)
    public Account getAccountById(long id) {
        return jdbcTemplate.queryForObject("select * from account_v where id = ?", accountRowMapper, id);
    }
}
