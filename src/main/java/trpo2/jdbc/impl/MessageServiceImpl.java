package trpo2.jdbc.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import trpo2.core.dto.Account;
import trpo2.core.dto.Message;
import trpo2.core.dto.MessageColor;
import trpo2.core.service.MessageService;
import trpo2.jdbc.base.BaseService;
import trpo2.util.CommonUtils;

import java.util.Date;
import java.util.List;

/**
 * @author Nechaev Alexander
 */
@Service("messageService")
public class MessageServiceImpl extends BaseService implements MessageService {

    private static final RowMapper<Message> messageRowMapper = (rs, i) -> {
        Message message = new Message();

        message.setId(rs.getLong("id"));
        message.setContent(rs.getString("content"));
        message.setPostDate(rs.getTimestamp("post_date"));

        Account author = new Account();
        author.setId(rs.getLong("author_id"));
        author.setLogin(rs.getString("author_login"));
        message.setAuthor(author);

        MessageColor messageColor = new MessageColor();
        messageColor.setId(rs.getLong("message_color_id"));
        messageColor.setHexColor(rs.getString("message_color_hex_color"));
        messageColor.setName(rs.getString("message_color_name"));
        message.setMessageColor(messageColor);

        return message;
    };

    private static final RowMapper<MessageColor> messageColorRowMapper = (rs, i) -> {
        MessageColor messageColor = new MessageColor();

        messageColor.setId(rs.getLong("id"));
        messageColor.setHexColor(rs.getString("hex_color"));
        messageColor.setName(rs.getString("name"));

        return messageColor;
    };

    @Override
    @Transactional(readOnly = true)
    public List<Message> getMessages() {
        return jdbcTemplate.query("select * from message_v order by post_date desc", messageRowMapper);
    }

    @Override
    @Transactional
    public long postMessage(Message message) {
        if (!isNew(message)) {
            // post значит, что для новых строк - insert, для обновленных - update
            throw new UnsupportedOperationException("Обновление данных не реализовано");
        }

        return jdbcTemplate.insertReturnId(
                "insert into messages(author_id, content, post_date, message_color_id) values(?, ?, ?, ?)",
                CommonUtils.getId(message.getAuthor()),
                message.getContent(),
                CommonUtils.nvl(message.getPostDate(), new Date()),
                CommonUtils.getId(message.getMessageColor())
        );
    }

    @Override
    @Transactional(readOnly = true)
    public List<MessageColor> getMessageColors() {
        return jdbcTemplate.query("select * from message_color_v", messageColorRowMapper);
    }
}
