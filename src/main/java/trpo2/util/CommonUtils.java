package trpo2.util;

import trpo2.core.dto.base.BaseDTO;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * @author Nechaev Alexander
 */
public class CommonUtils {

    public static <T> boolean equals(T a, T b) {
        return a == null ? b == null : a.equals(b);
    }

    public static <T extends BaseDTO> boolean equalsById(T a, T b) {
        return a == null ? b == null : equals(a.getId(), b.getId());
    }

    public static <T extends BaseDTO> Long getId(T obj) {
        return obj != null ? obj.getId() : null;
    }

    public static <T> T nvl(T obj, T ifNull) {
        return obj != null ? obj : ifNull;
    }

    public static String encryptMD5(String input) {
        try {
            return encryptMD5(input.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String encryptMD5(byte[] input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input);
            BigInteger number = new BigInteger(1, messageDigest);
            String result = number.toString(16);
            while (result.length() < 32)
                result = '0' + result;
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
