package trpo2.core.dto;

import trpo2.core.dto.base.BaseDTO;

/**
 * @author Nechaev Alexander
 */
public class MessageColor extends BaseDTO {

    private String hexColor;
    private String name;

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
