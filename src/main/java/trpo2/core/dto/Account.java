package trpo2.core.dto;

import trpo2.core.dto.base.BaseDTO;

/**
 * @author Nechaev Alexander
 */
public class Account extends BaseDTO {

    private String login;
    private String passwordHash;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
