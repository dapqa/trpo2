package trpo2.core.dto;

import java.io.Serializable;

/**
 * @author Nechaev Alexander
 */
public class AuthToken implements Serializable {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
