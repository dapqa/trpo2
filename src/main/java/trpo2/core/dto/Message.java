package trpo2.core.dto;

import trpo2.core.dto.base.BaseDTO;

import java.util.Date;

/**
 * @author Nechaev Alexander
 */
public class Message extends BaseDTO {

    private Account author;
    private String content;
    private Date postDate;
    private MessageColor messageColor;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Message message = (Message) o;

        if (author != null ? !author.equals(message.author) : message.author != null) return false;
        if (content != null ? !content.equals(message.content) : message.content != null) return false;
        if (postDate != null ? !postDate.equals(message.postDate) : message.postDate != null) return false;
        return messageColor != null ? messageColor.equals(message.messageColor) : message.messageColor == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (postDate != null ? postDate.hashCode() : 0);
        result = 31 * result + (messageColor != null ? messageColor.hashCode() : 0);
        return result;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public MessageColor getMessageColor() {
        return messageColor;
    }

    public void setMessageColor(MessageColor messageColor) {
        this.messageColor = messageColor;
    }
}
