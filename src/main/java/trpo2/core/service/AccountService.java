package trpo2.core.service;

import trpo2.core.dto.Account;

/**
 * @author Nechaev Alexander
 */
public interface AccountService {

    /**
     * Регистрирует пользователя в системе.
     * @param username Имя пользователя
     * @param password Пароль в открытом виде
     * @throws IllegalArgumentException Если пользователь с таким именем уже существует
     */
    Long signUp(String username, String password);

    /**
     * Аутентификация пользователя в системе.
     * @param username Имя пользователя
     * @param password Пароль в открытом виде
     * @throws IllegalArgumentException Если неверная пара логин-пароль
     */
    Long signIn(String username, String password);

    /**
     * Начитать аккаунт по его ИД
     * @param id ИД аккаунта
     * @return Аккаунт
     */
    Account getAccountById(long id);

}
