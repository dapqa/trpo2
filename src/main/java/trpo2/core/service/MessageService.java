package trpo2.core.service;

import trpo2.core.dto.Message;
import trpo2.core.dto.MessageColor;

import java.util.List;

/**
 * @author Nechaev Alexander
 */
public interface MessageService {

    List<Message> getMessages();

    long postMessage(Message message);

    List<MessageColor> getMessageColors();

}
